# 全自动安装98五笔

## 原生码表

`fctixt5` 自带的 `fcitx5-table` ，基于 `fcitx5` 原生数据库实现。可以实现简单的五笔功能交互，体积小、简明易用。

您的发行版应已安装 `curl` 这个常用工具，如果没有，可以尝试通过发行版官方仓库安装。

这里，给出 `Debian` 系列的命令：`sudo apt install curl`

现在，您可以使用下面的一条命令，将「98五笔·单字」、「98五笔·含词」、「98五笔·超集」、「98五笔·拼音混输」安装到您的电脑中。

```
curl -sSL https://raw.gitmirror.com/yanhuacuo/98wubi/refs/heads/master/shell/test.sh | bash
```

## 中州韵

基于 `fcitx5-rime` 实现的98五笔配置，可以实现最丰富的五笔功能。

一键安装：

```
curl -sSL https://raw.gitmirror.com/yanhuacuo/98wubi/refs/heads/master/shell/rime.sh | bash
```

# fcitx5-rime 98五笔功能介绍


### 组合键

| 组合键值 | 功能 |
|:-:|:-:|
| ctrl+` |  切换方案 |
| ctrl+shift+F | 繁简转换 |
| ctrl+shift+H | 拆分显隐 |
| ctrl+shift+J | 三重注解 | 
| ctrl+shift+U | 字集过滤 |
| ctrl+shift+K | 单字模式 |

### 功能键

| 触发键值 | 功能 |
|:-:|:-:|
| help | 帮助菜单 |
| mode | 切换方案 |
| next | 功能切换 |
| ~ | 以形查音 |
| z | 临时拼音、上屏历史 | 
| / | symbols符号系统 |
| jq | 节气 |
| date | 日期 |
| week | 周历 |

### lua字符串

大写字母触发 `lua` 功能模块

- 五笔98版字根

> 形如 `Ddd` 或 `DDD` 以大写字母开头的，三个同名字母，即触发。

- 金额转换

> 任意大写字母开头+金额数值

- 日期转换

> 任意大写字母开头+日期数值

# Gnome 环境下的 ibus-rime 安装

```
curl -sSL https://raw.gitmirror.com/yanhuacuo/98wubi/refs/heads/master/shell/ibus-rime.sh | bash
```

# 鼠须管98五笔一键安装

## 在安装过鼠须管程序后，执行下面的命令

```
curl -sSL https://raw.gitmirror.com/yanhuacuo/98wubi/refs/heads/master/shell/squirrel.sh | bash
```

### 联系

- [98五笔资源库](https://wb98.gitee.io/)
- [五笔小筑](https://wubi98.gitee.io/)
- [GitHub](https://github.com/yanhuacuo/98wubi-tables)

### 邮箱

wubixiaozhu@126.com

